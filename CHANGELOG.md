# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.5](https://bitbucket.org/jorgnyg/unisnitt/compare/v0.0.4...v0.0.5) (2020-05-21)

### [0.0.4](https://bitbucket.org/jorgnyg/unisnitt/compare/v0.0.3...v0.0.4) (2020-05-19)

### [0.0.3](https://bitbucket.org/jorgnyg/unisnitt/compare/v0.0.2...v0.0.3) (2020-05-15)

### [0.0.2](https://bitbucket.org/jorgnyg/unisnitt/compare/v0.0.1...v0.0.2) (2020-05-11)

### [0.0.1](https://bitbucket.org/jorgnyg/unisnitt/compare/v0.0.0...v0.0.1) (2020-05-11)

## [0.0.0](https://bitbucket.org/jorgnyg/unisnitt/compare/v1.0.0...v0.0.0) (2020-05-11)


### Features

* add standard version for releases ([1ee2204](https://bitbucket.org/jorgnyg/unisnitt/commit/1ee2204f5b2aff269831ecee4e05caf10c1d59bd))
* add standard version for releases ([8418243](https://bitbucket.org/jorgnyg/unisnitt/commit/84182437b181200ff97e6aaecec8ab0aa888fee5))
