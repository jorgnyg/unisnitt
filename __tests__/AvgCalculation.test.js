import React from "react";
import calculateAvg from "../helpers/calculateAvg";
import Grade from "../models/grade";

test("avg of test data to be 3.56", () => {
	const testData = [
		new Grade(1, 5, "A"),
		new Grade(2, 15, "B"),
		new Grade(3, 25, "C"),
	];

	expect(calculateAvg(testData)).toBeCloseTo(3.56);
});
