import gradesToNum from "./gradesToNum";

export default function (userInputs) {
	let sumGrades = 0;
	let sumStp = 0;

	for (let result of userInputs) {
		sumStp += parseInt(result.stp);
		sumGrades += parseInt(result.stp) * gradesToNum(result.grade);
	}

	let avgGrade = sumGrades / sumStp;

	return avgGrade;
}
