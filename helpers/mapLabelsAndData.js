export default mapLabelsAndData = (
	dataToHandle,
	selectedResults,
	firstFetch
) => {
	let baseData = { a: 0, b: 0, c: 0, d: 0, e: 0, f: 0 }; // not handeling G and H
	let validGradeData = ['a', 'b', 'c', 'd', 'e', 'f'];

	// checking if first fetch or changing grade
	if (firstFetch === true) {
		if (typeof dataToHandle[0]['g'] !== 'undefined') {
			baseData = { g: 0, h: 0 };
			validGradeData = ['g', 'h'];
		}
	} else if (firstFetch === false) {
		if (typeof dataToHandle['g'] !== 'undefined') {
			baseData = { g: 0, h: 0 };
			validGradeData = ['g', 'h'];
		}
	}

	const gradeLabels = [];
	const gradeData = [];

	dataToHandle = Object.assign(baseData, selectedResults);

	for (const key in dataToHandle) {
		if (validGradeData.includes(key.toLowerCase())) {
			gradeLabels.push(key.toUpperCase());
			gradeData.push(dataToHandle[key]);
		}
	}

	return {
		labels: gradeLabels,
		data: gradeData,
	};
};
