export default sortNsdDataByYear = (results) => {
	const resultsByYear = [];

	for (const key in results) {
		const resultYear = results[key]['Årstall'];

		const checkYear = (obj) => obj.semester_code === resultYear;

		const grade = results[key]['Karakter'];
		const count = results[key]['Antall kandidater totalt'];

		if (resultsByYear.some(checkYear)) {
			for (const resultByYear in resultsByYear) {
				if (
					resultsByYear[resultByYear]['semester_code'] === resultYear
				) {
					resultsByYear[resultByYear][grade.toLowerCase()] = count;
				}
			}
		} else {
			resultsByYear.push({
				semester_code: results[key]['Årstall'],
				[grade.toLowerCase()]: count,
			});
		}
	}

	return resultsByYear;
};
