export default searchNsd = (courseCodeInput, selectedUniInput) => {
	let courseNameResponse;
	let courseCodeResponse;

	const courseNameQuery = (courseCode, selectedUni) => {
		return {
			tabell_id: 208,
			api_versjon: 1,
			statuslinje: 'N',
			begrensning: '10000',
			kodetekst: 'J',
			desimal_separator: '.',
			variabler: ['*'],
			sortBy: [
				'Semester',
				'Institusjonskode',
				'Avdelingskode',
				'Emnekode',
			],
			filter: [
				{
					variabel: 'Institusjonskode',
					selection: {
						filter: 'item',
						values: [`${selectedUni}`],
					},
				},
				{
					variabel: 'Emnekode',
					selection: {
						filter: 'like',
						values: [`${courseCode}-%`],
						exclude: [''],
					},
				},
				{
					variabel: 'Årstall',
					selection: {
						filter: 'top',
						values: ['1'],
						exclude: [''],
					},
				},
			],
		};
	};

	const gradesQuery = (courseCode, selectedUni) => {
		return {
			tabell_id: 308,
			api_versjon: 1,
			statuslinje: 'N',
			begrensning: '1000',
			kodetekst: 'N',
			desimal_separator: '.',
			groupBy: ['Institusjonskode', 'Emnekode', 'Karakter', 'Årstall'],
			sortBy: ['Årstall'],
			filter: [
				{
					variabel: 'Institusjonskode',
					selection: {
						filter: 'item',
						values: [`${selectedUni}`], // UiT / UiO
					},
				},
				{
					variabel: 'Emnekode',
					selection: {
						filter: 'item',
						values: [`${courseCode}`], // `${courseCode}%` // vurdere å bruke course code som hentes fra første spørring..
					},
				},
			],
		};
	};

	// Call the API
	return fetch(
		'https://api.nsd.no/dbhapitjener/Tabeller/hentJSONTabellData',
		{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(
				courseNameQuery(courseCodeInput, selectedUniInput)
			),
		}
	)
		.then(function (response) {
			if (response.ok) {
				return response.json();
			} else {
				return Promise.reject(JSON.stringify(response));
			}
		})
		.then(function (data) {
			// Store the post data to a variable

			courseNameResponse = data[0]['Emnenavn'];
			courseCodeResponse = data[0]['Emnekode']; //denne brukes i siste spørring?

			// Fetch another API
			return fetch(
				'https://api.nsd.no/dbhapitjener/Tabeller/hentJSONTabellData',
				{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(
						gradesQuery(courseCodeResponse, selectedUniInput)
					),
				}
			);
		})
		.then(function (response) {
			if (response.ok) {
				return response.json();
			} else {
				return Promise.reject(JSON.stringify(response));
			}
		})
		.then(function (results) {
			return [
				`${courseCodeResponse.slice(0, -2)} - ${courseNameResponse}`,
				results,
			];
		})
		.catch(function (error) {
			console.log(error);
			return new Error('Something went wrong');
		});
};
