export default class SavedResult {
	constructor(id, inputs, avg, date) {
		this.id = id;
		this.inputs = inputs;
		this.avg = avg;
		this.date = date;
	}
}
