import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import GradesScreen from '../screens/GradesScreen';
import SavedGradesScreen from '../screens/SavedGradesScreen';
import SearchGradesScreen from '../screens/SearchGradesScreen';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import IconsHeaderButton from '../components/UI/IconsHeaderButton';
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import * as CustomTheme from '../constants/custom-theme';

const DrawerMenuButton = (navigation) => (
	<HeaderButtons HeaderButtonComponent={IconsHeaderButton}>
		<Item
			title="menu"
			iconName="md-menu"
			onPress={() => navigation.toggleDrawer()}
		/>
	</HeaderButtons>
);

const GradesScreenStack = createStackNavigator();
const GradesStack = ({ navigation }) => {
	return (
		<GradesScreenStack.Navigator
			screenOptions={{
				headerLeft: () => DrawerMenuButton(navigation),
			}}
		>
			<GradesScreenStack.Screen
				name="Grades"
				component={GradesScreen}
				options={{ title: 'Snittkalkulator' }}
			/>
		</GradesScreenStack.Navigator>
	);
};

const SavedGradesStackScreen = createStackNavigator();
const SavedGradesStack = ({ navigation }) => {
	return (
		<SavedGradesStackScreen.Navigator
			screenOptions={{ headerLeft: () => DrawerMenuButton(navigation) }}
		>
			<SavedGradesStackScreen.Screen
				name="SavedGradesScreen"
				component={SavedGradesScreen}
				options={{ title: 'Lagrede resultater' }}
			/>
		</SavedGradesStackScreen.Navigator>
	);
};

const SearchGradesStackScreen = createStackNavigator();
const SearchGradesStack = ({ navigation }) => {
	return (
		<SearchGradesStackScreen.Navigator
			screenOptions={{ headerLeft: () => DrawerMenuButton(navigation) }}
		>
			<SearchGradesStackScreen.Screen
				name="SearchGradesScreen"
				component={SearchGradesScreen}
				options={{ title: 'Statistikk' }}
			/>
		</SearchGradesStackScreen.Navigator>
	);
};

const Drawer = createDrawerNavigator();
const GradesNavigator = () => {
	return (
		<NavigationContainer>
			<Drawer.Navigator
				initialRouteName="GradesStack"
				drawerContentOptions={{
					activeTintColor: CustomTheme['color-primary-500'],
					activeBackgroundColor:
						CustomTheme['color-primary-transparent-100'],
				}}
			>
				<Drawer.Screen
					name="GradesStack"
					component={GradesStack}
					options={{
						title: 'Snittkalkulator',
						drawerIcon: (props) => (
							<MaterialCommunityIcons
								size={23}
								color={props.color}
								name="calculator-variant"
							/>
						),
					}}
				/>
				<Drawer.Screen
					name="SearchGradesStack"
					component={SearchGradesStack}
					options={{
						title: 'Statistikk',
						drawerIcon: (props) => (
							<Ionicons
								size={23}
								color={props.color}
								name="ios-stats"
							/>
						),
					}}
				/>
				<Drawer.Screen
					name="SavedGradesStack"
					component={SavedGradesStack}
					options={{
						title: 'Lagrede resultater',
						drawerIcon: (props) => (
							<Ionicons
								size={23}
								color={props.color}
								name="ios-star"
							/>
						),
					}}
				/>
			</Drawer.Navigator>
		</NavigationContainer>
	);
};

export default GradesNavigator;
