export const ADD_INPUT = "ADD_INPUT";
export const DELETE_INPUT = "DELETE_INPUT";
export const CHANGE_STP = "CHANGE_STP";
export const CHANGE_GRADE = "CHANGE_GRADE";

export const LOAD_SAVED_RESULT = "LOAD_SAVED_RESULT";

export const changeStp = (id, text) => {
	return async (dispatch) => {
		try {
			dispatch({
				type: CHANGE_STP,
				data: {
					inputId: id,
					text: text,
				},
			});
		} catch (err) {
			throw err;
		}
	};
};

export const changeGrade = (id, option) => {
	return async (dispatch) => {
		try {
			dispatch({
				type: CHANGE_GRADE,
				data: {
					inputId: id,
					optionText: option,
				},
			});
		} catch (err) {
			throw err;
		}
	};
};

export const addInput = () => {
	return async (dispatch, getState) => {
		const someVal = getState().inputs.inputs;

		try {
			dispatch({
				type: ADD_INPUT,
			});
		} catch (err) {
			throw err;
		}
	};
};

export const deleteInput = (inputId) => {
	return async (dispatch) => {
		try {
			dispatch({
				type: DELETE_INPUT,
				data: {
					inputId: inputId,
				},
			});
		} catch (err) {
			throw err;
		}
	};
};

export const loadSavedResult = (inputs) => {
	return async (dispatch, getState) => {
		try {
			dispatch({
				type: LOAD_SAVED_RESULT,
				data: {
					resultToLoad: inputs,
				},
			});
		} catch (err) {
			throw err;
		}
	};
};
