export const SAVE_RESULT = "SAVE_RESULT";
export const DELETE_RESULT = "DELETE_RESULT";

export const saveResult = (currentUserInputs) => {
	return async (dispatch) => {
		try {
			dispatch({
				type: SAVE_RESULT,
				data: {
					currentUserInputs: currentUserInputs,
				},
			});
		} catch (err) {
			throw err;
		}
	};
};

export const deleteResult = (id) => {
	return async (dispatch) => {
		try {
			dispatch({
				type: DELETE_RESULT,
				data: {
					id: id,
				},
			});

			//return Promise.resolve();
		} catch (err) {
			console.log(err);

			throw err;
		}
	};
};
