import {
	ADD_INPUT,
	DELETE_INPUT,
	CHANGE_GRADE,
	CHANGE_STP,
	LOAD_SAVED_RESULT,
} from "../actions/inputs";
import Grade from "../../models/grade";

const initialState = {
	inputs: [new Grade(Math.random(), "", "")],
};

export default (state = initialState, action) => {
	switch (action.type) {
		case CHANGE_STP:
			const updatedStp = state.inputs.map((obj) => {
				if (obj.id === action.data.inputId)
					return {
						...obj,
						stp: action.data.text.replace(/[^0-9\.,]/g, ""),
					};

				return obj;
			});
			return {
				...state,
				inputs: updatedStp,
			};
		case CHANGE_GRADE:
			const updatedGrade = state.inputs.map((obj) => {
				if (obj.id === action.data.inputId)
					return {
						...obj,
						grade: action.data.optionText,
					};

				return obj;
			});
			return {
				...state,
				inputs: updatedGrade,
			};
		case ADD_INPUT:
			const newInput = new Grade(Math.random(), "", "");

			return {
				...state,
				inputs: [newInput, ...state.inputs],
			};
		case DELETE_INPUT:
			return {
				...state,
				inputs: state.inputs.filter(
					(inputObj) => inputObj.id !== action.data.inputId
				),
			};

		case LOAD_SAVED_RESULT:
			const newInputs = action.data.resultToLoad;

			return {
				...state,
				inputs: newInputs,
			};
	}

	return state;
};
