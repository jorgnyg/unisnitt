import { SAVE_RESULT, DELETE_RESULT } from "../actions/savedResults";
import SavedResult from "../../models/savedResult";
import calculateAvg from "../../helpers/calculateAvg";
import moment from "moment";
import "moment/locale/nb";

const initialState = {
	savedResults: [],
};

export default (state = initialState, action) => {
	switch (action.type) {
		case SAVE_RESULT:
			const inputsAvg = calculateAvg(action.data.currentUserInputs); // samme her. når avg er hentet, kjør nytt resultat.
			const newSavedResult = new SavedResult(
				Math.random(),
				action.data.currentUserInputs,
				inputsAvg.toFixed(2),
				moment().locale("nb").format("LLL")
			);
			return {
				...state,
				savedResults: [newSavedResult, ...state.savedResults], //state.savedResults.concat(newSavedResult), // action for å slette lagrede resultater
			};

		case DELETE_RESULT:
			return {
				...state,
				savedResults: state.savedResults.filter(
					(resultObj) => resultObj.id !== action.data.id
				),
			};
	}

	return state;
};
