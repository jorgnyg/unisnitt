import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	StyleSheet,
	TouchableWithoutFeedback,
	Keyboard,
	Alert,
} from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import IconsHeaderButton from '../components/UI/IconsHeaderButton';
import { Button, Layout } from '@ui-kitten/components';
import GradesResult from '../components/GradesResult';
import GradesForm from '../components/GradesForm';
import calculateAvg from '../helpers/calculateAvg';
import * as savedResultsActions from '../store/actions/savedResults';

const GradesScreen = ({ navigation, route }) => {
	const [avg, setAvg] = useState('Legg til studiepoeng og karakter');
	const userInputs = useSelector((state) => state.inputs.inputs);

	const avgGradeFromSave = route.params?.avgGradeFromSave;

	const dispatch = useDispatch();

	const calculateResultsHandler = () => {
		let avgGrade = calculateAvg(userInputs); // burde ha en form for promise her. hvis går gjennom -> setAvg
		setAvg(avgGrade.toFixed(2));
	};

	const saveResultsHandler = () => {
		if (isNaN(calculateAvg(userInputs))) {
			Alert.alert(
				'Feil i input',
				'Sjekk at input for karakter og studiepoeng stemmer',
				[{ text: 'OK', style: 'cancel' }]
			);
			return;
		} else {
			dispatch(savedResultsActions.saveResult(userInputs)).then(() => {
				Alert.alert(
					'Resultatene ble lagret',
					'Vil du gå til oversikten over lagrede resultater?',
					[
						{
							text: 'Nei',
						},
						{
							text: 'Ja',
							onPress: () =>
								navigation.navigate('SavedGradesStack', {
									screen: 'SavedGradesScreen',
								}),
						},
					],
					{ cancelable: false }
				);
			});
		}
	};

	// useEffect for å oppdatere avg state
	// bug: last inn lagret resultat, endre resultat, regn ut og last inn samme lagret resultat. blir ikke lagret
	useEffect(() => {
		if (avgGradeFromSave != undefined) {
			setAvg(avgGradeFromSave);
		}
	}, [avgGradeFromSave, route]);

	useEffect(() => {
		navigation.setOptions({
			headerRight: () => (
				<HeaderButtons HeaderButtonComponent={IconsHeaderButton}>
					<Item
						title="save"
						iconName="ios-save"
						onPress={saveResultsHandler}
					/>
				</HeaderButtons>
			),
		});
	}, [navigation, userInputs]);

	return (
		<TouchableWithoutFeedback onPress={Keyboard.dismiss}>
			<Layout style={styles.screen}>
				<GradesResult avg={avg}>
					<Button onPress={calculateResultsHandler}>Regn ut</Button>
				</GradesResult>
				<GradesForm />
			</Layout>
		</TouchableWithoutFeedback>
	);
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
	},
});

export default GradesScreen;
