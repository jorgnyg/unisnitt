import React, { useState, useEffect } from 'react';
import {
	StyleSheet,
	TouchableWithoutFeedback,
	Keyboard,
	KeyboardAvoidingView,
	SafeAreaView,
	ActivityIndicator,
} from 'react-native';
import { Button, Layout, Input, Text } from '@ui-kitten/components';
import GradesGraph from '../components/GradesGraph';
import StatsNav from '../components/UI/StatsNav';
import searchNsd from '../helpers/api/searchNsd';
import SearchSelect from '../components/UI/SearchSelect';
import sortNsdDataByYear from '../helpers/sortNsdDataByYear';
import DropDownHolder from '../helpers/DropDownHolder';
import mapLabelsAndData from '../helpers/mapLabelsAndData';

const SearchGradesScreen = () => {
	const [selectedLabels, setSelectedLabels] = useState([
		'A',
		'B',
		'C',
		'D',
		'E',
		'F',
	]);
	const [selectedData, setSelectedData] = useState([0, 0, 0, 0, 0, 0]);
	const [selectedCourseCodeAndName, setSelectedCourseCodeAndName] = useState(
		'Legg til gyldig emnekode og søk'
	);
	const [selectedSemester, setSelectedSemester] = useState('SEMESTER');
	const [loadedData, setLoadedData] = useState([]);
	const [courseCodeInput, setCourseCodeInput] = useState('');
	const [selectedIndex, setSelectedIndex] = useState(1);
	const [selectedUni, setSelectedUni] = useState(1150); // NTNU as init value
	const [isLoading, setisLoading] = useState(false);

	const changeSemester = (nav) => {
		let newSelectedIndex =
			nav === 'prev' ? selectedIndex + 1 : selectedIndex - 1;

		const selectedResults =
			loadedData[loadedData.length - newSelectedIndex];

		let dataToHandle = selectedResults;

		const labelsAndData = mapLabelsAndData(
			dataToHandle,
			selectedResults,
			false
		);

		setSelectedItemsHandler(
			selectedResults['semester_code'], // Depens on the source! "semester_code" for grades.no & "year" from
			labelsAndData.labels,
			labelsAndData.data,
			newSelectedIndex
		);
	};

	const nsdDataHandler = (courseName, response) => {
		const resultsByYear = sortNsdDataByYear(response);

		const startIndex = 1;

		const latestResults = resultsByYear[resultsByYear.length - startIndex];

		const labelsAndData = mapLabelsAndData(
			resultsByYear,
			latestResults,
			true
		);

		setLoadedData(resultsByYear);
		setSelectedCourseCodeAndName(courseName);
		setSelectedItemsHandler(
			latestResults['semester_code'],
			labelsAndData.labels,
			labelsAndData.data,
			startIndex
		);
	};

	const searchNsdHandler = () => {
		setisLoading(true);
		searchNsd(courseCodeInput, selectedUni).then((response) => {
			setisLoading(false);
			response instanceof Error
				? DropDownHolder.dropDown.alertWithType(
						'info',
						'Fant ikke emnet',
						'Sjekk at du har valgt riktig institusjon og at emenkoden er riktig.'
				  )
				: nsdDataHandler(response[0], response[1]);
		});
	};

	const setSelectedItemsHandler = (semester, labels, data, index) => {
		setSelectedSemester(semester);
		setSelectedLabels(labels);
		setSelectedData(data);
		setSelectedIndex(index);
	};

	return (
		<TouchableWithoutFeedback onPress={Keyboard.dismiss}>
			<SafeAreaView style={styles.screen}>
				<KeyboardAvoidingView
					behavior={Platform.OS === 'ios' ? 'padding' : {}}
				>
					<Layout style={styles.selectAndInput}>
						<SearchSelect
							style={styles.select}
							selectedUni={selectedUni}
							changeUni={(uniName) => setSelectedUni(uniName)}
						/>

						<Input
							style={styles.input} // setSelectedUni(uniName)
							//height={75}
							//size="small"
							placeholder="Emnekode"
							value={courseCodeInput}
							onChangeText={(nextValue) =>
								setCourseCodeInput(nextValue)
							}
						/>
						<Button
							appearance="outline"
							onPress={searchNsdHandler}
							size="small"
						>
							Søk
						</Button>
					</Layout>
					<ActivityIndicator
						size={50}
						animating={isLoading}
						color="#074E89"
					/>
					<Layout style={styles.content}>
						<Layout>
							<Text style={{ textAlign: 'center' }} category="h5">
								{selectedCourseCodeAndName}
							</Text>
						</Layout>

						<GradesGraph
							selectedLabels={selectedLabels}
							selectedData={selectedData}
						/>

						{loadedData.length !== 0 && (
							<Layout style={styles.nav}>
								<StatsNav
									selectedIndex={selectedIndex}
									loadedDataLength={loadedData.length}
									selectedSemester={selectedSemester}
									changeSemester={changeSemester}
								/>
							</Layout>
						)}
					</Layout>
				</KeyboardAvoidingView>
			</SafeAreaView>
		</TouchableWithoutFeedback>
	);
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'white',
	},
	content: {
		flex: 1,
		width: '95%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	selectAndInput: {
		alignItems: 'center',
		flexDirection: 'row',
	},
	nav: {
		marginTop: 10,
		flex: 0.25,
		justifyContent: 'flex-start',
		alignItems: 'center',
		flexDirection: 'row',
	},
	input: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'row',
	},
});

export default SearchGradesScreen;
