import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { StyleSheet, View, Dimensions } from "react-native";
import {
	Layout,
	Divider,
	List,
	ListItem,
	Button,
	Text,
} from "@ui-kitten/components";

import {
	LineChart
} from "react-native-chart-kit";
import * as inputsActions from "../store/actions/inputs";
import * as savedResultsActions from "../store/actions/savedResults";

const data = new Array(8).fill({
	title: "Item",
	description: "Description for Item",
});

const SavedGradesScreen = ({ navigation }) => {
	const savedResults = useSelector(
		(state) => state.savedResults.savedResults
	);


	const dispatch = useDispatch();
	const handleLoadSavedResult = (inputs, avg) => {
		dispatch(inputsActions.loadSavedResult(inputs)).then(() => {
			navigation.navigate("Grades", { avgGradeFromSave: avg });
		});
	};

	const handleDeleteSavedResult = (id) => {
		dispatch(savedResultsActions.deleteResult(id));
	};

	const renderItem = ({ item, index }) => (
		<ListItem
			title={`${item.inputs.length} resultater med gjennomsnitt på ${item.avg}`}
			description={`Lagret ${item.date} `}
			onPress={() => handleLoadSavedResult(item.inputs, item.avg)}
			accessoryRight={() => (
				<Button
					status="danger"
					appearance="outline"
					size="tiny"
					onPress={() => handleDeleteSavedResult(item.id)}
				>
					FJERN
				</Button>
			)}
		/>
	);

	if (savedResults.length <= 0) {
		return (
			<Layout level="2" style={styles.noResultsScreen}>
				<Text>Ingen lagrede resultater</Text>
			</Layout>
		);
	}

	return (
		<Layout style={styles.screen}>
			<List
				style={styles.container}
				data={savedResults}
				ItemSeparatorComponent={Divider}
				renderItem={renderItem}
			/>
		</Layout>
	);
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
	},
	noResultsScreen: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
	},
});

export default SavedGradesScreen;
