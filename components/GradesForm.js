import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { StyleSheet, ScrollView, Platform } from 'react-native';
import { Layout, Card, Button, Select, Text } from '@ui-kitten/components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import GradeInput from '../components/UI/GradeInput';
import * as inputsActions from '../store/actions/inputs';
import { useHeaderHeight } from '@react-navigation/stack';

const GradeForm = () => {
	const userInputs = useSelector((state) => state.inputs.inputs);

	const dispatch = useDispatch();

	const changeStp = (id, text) => {
		dispatch(inputsActions.changeStp(id, text));
	};

	const changeGrade = (id, option) => {
		dispatch(inputsActions.changeGrade(id, option));
	};

	const newUserInput = () => {
		dispatch(inputsActions.addInput());
	};

	const delUserInput = (id) => {
		dispatch(inputsActions.deleteInput(id));
	};

	return (
		<Layout style={styles.container}>
			<Card disabled={true} style={styles.gradeCard}>
				<Button
					style={styles.newGradeBtn}
					onPress={newUserInput}
					appearance="outline"
				>
					Ny karakter
				</Button>
				{Platform.OS === 'ios' && (
					<KeyboardAwareScrollView
						showsVerticalScrollIndicator={false}
						extraScrollHeight={-useHeaderHeight() + 10}
					>
						{userInputs.map((input) => (
							<GradeInput
								key={input.id}
								stpChange={(text) => changeStp(input.id, text)}
								stpValue={input.stp}
								gradeChange={(option) =>
									changeGrade(input.id, option)
								}
								gradeValue={input.grade}
								delClick={() => delUserInput(input.id)}
								disableDelete={
									userInputs.length < 2 ? true : false
								}
							/>
						))}
					</KeyboardAwareScrollView>
				)}

				{Platform.OS === 'android' && (
					<ScrollView showsVerticalScrollIndicator={false}>
						{userInputs.map((input) => (
							<GradeInput
								key={input.id}
								stpChange={(text) => changeStp(input.id, text)}
								stpValue={input.stp}
								gradeChange={(option) =>
									changeGrade(input.id, option)
								}
								gradeValue={input.grade}
								delClick={() => delUserInput(input.id)}
								disableDelete={
									userInputs.length < 2 ? true : false
								}
							/>
						))}
					</ScrollView>
				)}
			</Card>
		</Layout>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: Platform.OS === 'android' ? 4 : 5,
		justifyContent: 'center',
		alignItems: 'center',
	},
	gradeCard: {
		minWidth: '75%',
		maxHeight: '75%',
	},
	newGradeBtn: {
		marginBottom: 15,
	},
});

export default GradeForm;
