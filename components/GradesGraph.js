import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { BarChart } from 'react-native-chart-kit';

const GradesGraph = (props) => {
	const { selectedLabels, selectedData } = props;

	return (
		<BarChart
			data={{
				labels: selectedLabels,
				datasets: [
					{
						data: selectedData,
					},
				],
			}}
			width={Dimensions.get('window').width * 0.95} // from react-native
			height={220}
			showTextTop={true}
			fromZero={true}
			yAxisInterval={1} // optional, defaults to 1
			chartConfig={{
				decimalPlaces: 1, // optional, defaults to 2dp
				backgroundColor: '#074E89',
				backgroundGradientFrom: '#3583B8',
				backgroundGradientTo: '#053C75',
				color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
				labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
				style: {
					borderRadius: 16,
				},
				propsForDots: {
					r: '6',
					strokeWidth: '2',
					stroke: '#074E89',
				},
			}}
			bezier
			style={{
				marginVertical: 8,
				borderRadius: 16,
			}}
		/>
	);
};

const styles = StyleSheet.create({});

export default GradesGraph;
