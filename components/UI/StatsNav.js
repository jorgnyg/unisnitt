import React from 'react';
import { StyleSheet } from 'react-native';
import { Button, Layout, Input, Text } from '@ui-kitten/components';
import { Ionicons } from '@expo/vector-icons';

const StatsNav = (props) => {
	const {
		selectedIndex,
		loadedDataLength,
		selectedSemester,
		changeSemester,
	} = props;

	return (
		<>
			<Button
				onPress={
					selectedIndex < loadedDataLength
						? () => changeSemester('prev')
						: null
				}
				appearance="ghost"
				size="small"
				accessoryLeft={() => (
					<Ionicons
						name="ios-arrow-dropleft"
						color={
							selectedIndex < loadedDataLength
								? '#074E89'
								: 'rgba(7, 78, 137, 0.4)'
						}
						size={30}
					/>
				)}
			/>

			<Layout style={{ width: 75 }}>
				<Text style={{ textAlign: 'center' }}>{selectedSemester}</Text>
			</Layout>
			<Button
				onPress={
					selectedIndex === 1 ? null : () => changeSemester('next') // disable if first index
				}
				appearance="ghost"
				size="small"
				accessoryLeft={() => (
					<Ionicons
						name="ios-arrow-dropright"
						color={selectedIndex === 1 ? 'rgba(7, 78, 137, 0.4)' : '#074E89'}
						size={30}
					/>
				)}
			/>
		</>
	);
};

const styles = StyleSheet.create({
	navAndInput: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	nav: {
		flex: 1,
		justifyContent: 'flex-start',
		alignItems: 'center',
		flexDirection: 'row',
	},
});

export default StatsNav;
