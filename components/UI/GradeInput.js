import React from 'react';
import {
	Layout,
	Input,
	Select,
	SelectItem,
	IndexPath,
	Button,
} from '@ui-kitten/components';
import { StyleSheet } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

const GradeInput = (props) => {
	const data = ['A', 'B', 'C', 'D', 'E'];

	const selectedIndex = data.findIndex((itm) => itm === props.gradeValue);

	const renderOption = (title) => <SelectItem key={title} title={title} />;

	return (
		<Layout style={styles.container}>
			<Layout style={styles.selectField}>
				<Select
					placeholder="Karakter"
					value={props.gradeValue}
					selectedIndex={
						selectedIndex === -1
							? null
							: new IndexPath(selectedIndex)
					}
					onSelect={(option) => props.gradeChange(data[option.row])}
				>
					{data.map(renderOption)}
				</Select>
			</Layout>
			<Layout style={styles.inputField}>
				<Input
					placeholder="Stp."
					keyboardType="decimal-pad"
					returnKeyType="done"
					onChangeText={(text) => props.stpChange(text)}
					value={props.stpValue}
				/>
			</Layout>

			<Layout style={styles.delete}>
				<Button
					size="tiny"
					style={{ minHeight: 40 }}
					appearance="ghost"
					onPress={props.disableDelete ? null : props.delClick}
					accessoryRight={() => (
						<Ionicons
							name="ios-remove-circle-outline"
							color={
								props.disableDelete
									? 'rgba(219, 45, 32, 0.4)'
									: '#DB2D20'
							}
							size={25}
							disabled={props.disableDelete}
						/>
					)}
				/>
			</Layout>
		</Layout>
	);
};

// inputs and delete are not aligned

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'row',
	},
	inputField: {
		flex: 1,
	},
	selectField: {
		flex: 1,
	},
	delete: {
		flex: 0,
	},
});

export default GradeInput;
