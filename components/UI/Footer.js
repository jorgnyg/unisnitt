import React from "react";
import { ThemeContext } from "../../context/theme-context";
import { StyleSheet } from "react-native";
import { Button, Layout } from "@ui-kitten/components";

const Footer = ({ theme }) => {
	const themeContext = React.useContext(ThemeContext);

	return (
		<Layout style={styles.container}>
			<Button
				appearance="outline"
				onPress={themeContext.toggleTheme}
				size="tiny"
			>
				{theme === "light" ? "Natt" : "Dag"}
			</Button>
		</Layout>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 0,
		flexDirection: "row",
	},
});

export default Footer;
