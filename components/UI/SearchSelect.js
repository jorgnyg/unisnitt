import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Select, SelectItem, IndexPath } from '@ui-kitten/components';

const uniData = [
	{ name: 'NTNU', code: 1150 },
	{ name: 'UiO', code: 1110 },
	{ name: 'UiB', code: 1120 },
	{ name: 'UiA', code: 1171 },
	{ name: 'NHH', code: 1240 },
];

const SearchSelect = (props) => {
	const { selectedUni, changeUni } = props;

	const selectedIndex = uniData.map((uni) => uni.code).indexOf(selectedUni);

	const renderOption = (name, code) => <SelectItem key={code} title={name} />;

	return (
		<View keyboardShouldPersistTaps style={{ flex: 1 }}>
			<Select
				style={styles.select}
				placeholder="Default"
				value={uniData[selectedIndex].name}
				selectedIndex={new IndexPath(selectedIndex)}
				onSelect={(option) => changeUni(uniData[option.row].code)}
			>
				{uniData.map((uni) => renderOption(uni.name, uni.code))}
			</Select>
		</View>
	);
};

const styles = StyleSheet.create({
	select: {
		flex: 1,
		alignItems: 'center',
		flexDirection: 'row',
	},
});

export default SearchSelect;
