import React, { useContext } from "react";
import * as CustomTheme from "../../constants/custom-theme";
import { HeaderButton } from "react-navigation-header-buttons";
import { Ionicons } from "@expo/vector-icons";

const IconsHeaderButton = (props) => {
	return (
		<HeaderButton
			{...props}
			IconComponent={Ionicons}
			iconSize={23}
			color={CustomTheme["color-primary-500"]}
		/>
	);
};

export default IconsHeaderButton;
