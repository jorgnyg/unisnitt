import React from 'react';
import { StyleSheet } from 'react-native';
import { Layout, Text, Card } from '@ui-kitten/components';
import * as CustomTheme from '../constants/custom-theme';

const GradeResult = ({ children, avg }) => (
	<Layout style={styles.container}>
		<Card disabled={true} style={{ minWidth: 285 }}>
			<Text style={{ textAlign: 'center' }} category="h1">
				Gjennomsnitt
			</Text>
			<Text
				style={{
					marginBottom: 10,
					textAlign: 'center',
					color:
						avg == 'NaN' ? CustomTheme['color-danger-500'] : null,
				}}
			>
				{avg == 'NaN' ? 'Sjekk at input stemmer' : avg}
			</Text>
			{children}
		</Card>
	</Layout>
);

const styles = StyleSheet.create({
	container: {
		flex: 2,
		justifyContent: 'center',
		alignItems: 'center',
	},
});

export default GradeResult;
