import React from 'react';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { persistStore, persistReducer } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import { AsyncStorage } from 'react-native';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { ThemeContext } from './context/theme-context';
import { mapping, light, dark } from '@eva-design/eva';
import { default as appTheme } from './constants/custom-theme';
import GradesNavigator from './navigation/GradesNavigator';
import Footer from './components/UI/Footer';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import inputsReducer from './store/reducers/inputs';
import savedResultsReducer from './store/reducers/savedResults';

import DropDownHolder from "./helpers/DropDownHolder";
import DropdownAlert from 'react-native-dropdownalert';

import { AppLoading } from 'expo';

const themes = { light, dark };

const App = () => {
	const [theme, setTheme] = React.useState('light');
	const toggleTheme = () => {
		const nextTheme = theme === 'light' ? 'dark' : 'light';
		setTheme(nextTheme);
	};

	const rootReducer = combineReducers({
		inputs: inputsReducer,
		savedResults: savedResultsReducer,
	});

	const persistConfig = {
		key: 'root',
		storage: AsyncStorage,
		blacklist: ['inputs'],
	};

	const persistedReducer = persistReducer(persistConfig, rootReducer);

	let store = createStore(persistedReducer, applyMiddleware(ReduxThunk));
	let persistor = persistStore(store);

	return (
		<Provider store={store}>
			<PersistGate loading={<AppLoading />} persistor={persistor}>
				<ThemeContext.Provider value={{ theme, toggleTheme }}>
					<IconRegistry icons={EvaIconsPack} />
					<ApplicationProvider
						mapping={mapping}
						theme={{ ...themes[theme], ...appTheme }}
					>
						<GradesNavigator />
						<DropdownAlert inactiveStatusBarStyle="dark-content" ref={(ref) => DropDownHolder.setDropDown(ref)} />
					</ApplicationProvider>
				</ThemeContext.Provider>
			</PersistGate>
		</Provider>
	);
};

export default App;

// expo upload:android --key <key> --track <track>
